#!/usr/bin/env python

# Copyright 2021(R) Perception Engine. All rights reserved

from __future__ import print_function
import rospy
import cv2

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import roslib

roslib.load_manifest('ros_image_overlay')


class RosImageOverlay:
    def __init__(self):
        rospy.init_node("ros_image_overlay", anonymous=True)

        overlay_image_path = rospy.get_param('~overlay_image_path', '')
        if overlay_image_path == "":
            rospy.logerr("Invalid overlay image.")
            exit(-1)

        rospy.loginfo("overlay_image_path: {}".format(overlay_image_path))
        self.overlay = cv2.imread(overlay_image_path, cv2.IMREAD_COLOR)

        self.__input_topic = rospy.get_param('~input_topic', 'image_raw')
        rospy.loginfo("Subscribed to: {} [sensor_msgs/Image]".format(self.__input_topic))
        rospy.Subscriber(self.__input_topic, Image, self.__image_callback)

        rospy.loginfo("Publishing to: {} [sensor_msgs/Image]".format("image_overlay"))
        self.__overlay_pub = rospy.Publisher("image_overlay", Image, queue_size=1)
        self.bridge = CvBridge()

        rospy.spin()

    def overlay_images(self, background, foreground):
        bg_dim = background.shape
        fg_dim = foreground.shape
        if bg_dim[0] != fg_dim[0] or bg_dim[1] != fg_dim[1]:
            rospy.logwarn_throttle(1, "Resizing. Image dimensions don't match BG W{},H{} FG W{},H{}.".format(
                                   bg_dim[1], bg_dim[0], fg_dim[1], fg_dim[0]))
            new_foreground = cv2.resize(foreground, (bg_dim[1], bg_dim[0]), interpolation=cv2.INTER_AREA)
        else:
            new_foreground = foreground

        added_image = cv2.addWeighted(background,
                                      0.5,
                                      new_foreground,
                                      0.5,
                                      1)
        return added_image

    def __image_callback(self, image_msg):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(image_msg, "bgr8")
        except CvBridgeError as e:
            print(e)

        added_image = self.overlay_images(cv_image, self.overlay)
        added_image = cv2.flip(added_image, 1)

        try:
            self.__overlay_pub.publish(self.bridge.cv2_to_imgmsg(added_image, "bgr8"))
        except CvBridgeError as e:
            print(e)



if __name__ == "__main__":
    filter_node = RosImageOverlay()
